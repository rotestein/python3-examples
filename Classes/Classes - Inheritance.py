# Super!
class Parent:
    def __init__(self, a = 0):
        self.a = a

class BadChild(Parent):
    def __init__(self, b = False):
        self.b = b

class GoodChild(Parent):
    # Need to have parent parameters as well
    def __init__(self, b = False, a = 0):
        self.b = b
        super().__init__(a) # Pass along a

def main():
    Good = GoodChild()
    Bad = BadChild()
    print(str(Good.b), str(Good.a))
    print(str(Bad.b), str(Bad.a))

main()
