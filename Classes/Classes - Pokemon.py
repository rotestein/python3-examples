class Pokemon:
    def __init__(self, name="?", level=2, type="?", damage=0, health=0):
        print("__init__() called!")
        self.name = name
        self.level = level
        self.type = type
        self.damage = damage
        self.health = health

    def __repr__(self):
        print("__repr__() called!")
        out = ""
        out += self.name + "\n"
        out += "Level: " + str(self.level) + "\n"
        out += "Type: " + self.type + "\n"
        out += "Damage: " + str(self.damage) + "\n"
        out += "Health: " + str(self.health) + "\n"
        return out

    def attack(self, other):
        print("attack() called!")
        multiplier = 1
        if self.type == "rock" and other.type == "flying":
            multiplier = 2
            print("It's super effective!")
        elif self.type == "normal" and other.type == "ghost":
            multiplier = 0
            print("It had no effect!")

        other.health -= self.damage * multiplier
