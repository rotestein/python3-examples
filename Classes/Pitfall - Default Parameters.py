# Python evaluates default parameters when the function is *defined*!
# Not when the function is called. So in the first class, all instances
# DontDoThis that take the default parameter will point to the *same*
# list. You can delay the evaluation using `None`.

class DontDoThis:
    def __init__(self, my_list = []):
        self.my_list = my_list


class DoThis:
    def __init__(self, my_list = None):
        if my_list == None:
            my_list = []

        self.my_list = my_list

a = DontDoThis()
b = DontDoThis()
a.my_list.append('whoops')
print(a.my_list)
print(b.my_list)
c = DontDoThis()
print(c.my_list)
