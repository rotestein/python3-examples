from ex1 import Pokemon

Ghastly = Pokemon("Ghastly", 10, "ghost", 30, 100)
Raticate = Pokemon("Raticate", 15, "normal", 45, 100)

print(Ghastly)
print(Raticate)

print(Ghastly.name, "attacks", Raticate.name)
Ghastly.attack(Raticate)
print(Raticate.name, "attacks", Ghastly.name)
Raticate.attack(Ghastly)

print(Ghastly)
print(Raticate)
