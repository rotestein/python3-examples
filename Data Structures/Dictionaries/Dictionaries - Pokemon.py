# Dictionaries can be complicated

Pokemon = {
    'name' : 'Charizard',
    'moves' : ['Flamethrower', 'Fly', 'Fire Blast', 'Bite'],
    'pp' : [15, 15, 5, 12],
    'nickname' : 'El Chupacabra',
    'stats' : {
        'health' : 100,
        'attack' : 212,
        'special attack' : 300,
        'defense' : 100,
        'special defense' : 80
    }
}

for key in Pokemon:
    print(key, ':', Pokemon[key])
