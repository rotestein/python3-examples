# With lists, specific elements are identified by position
# With dictionaries, they are identified by a key

# Remember the list of counters for dice rolls?
l = [0] * 11

# If I rolled a 4, I needed to subtract 2 to get the index
roll = 4
l[roll - 2] += 1

# We could instead use a dictionary
d = {2: 0, 3: 0, 4: 0, 5: 0, 6: 0, 7: 0, 8: 0, 9: 0, 10: 0, 11: 0, 12: 0}
d[roll] += 1

print(d.keys())
print(d.items())
print(d.values())
