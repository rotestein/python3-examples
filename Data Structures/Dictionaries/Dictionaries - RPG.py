# In dictionaries, keys can be ANY immutable object,
# but are commonly strings

Mage = {
    'mana' : 85,
    'health' : 100,
    'name' : 'Jake',
}

Thief = {
    'power' : 12,
    'health' : 90,
    'name' : 'Jeff'
}

def applyDamage(character, damage):
    character['health'] -= damage

print(Thief['name'], 'attacks', Mage['name'])
print(Thief['name'], 'does', Thief['power'], 'damage to', Mage['name'])
print(Mage['name'], 'health before damage:', Mage['health'])
applyDamage(Mage, Thief['power'])
print(Mage['name'], 'health after damage:', Mage['health'])
