# Examples of using list slicing to get a sub list/string

my_list = [1, 2, 3, 5, 7, 11, 13];
my_string = "what does the fox say?"

print(my_list[1:3]) # Get a sub list with items 1 and 2
print(my_string[5:9] == "does") # Get a substring with characters 5 - 8

print(my_string[10:]) # Get all characters from 11 to the end of the string
print(my_list[-3:-1]) # Get the last 3rd to last and 2nd to last elements of the list
