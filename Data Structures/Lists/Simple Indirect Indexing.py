# The number of the bucket can have a direct or indirect meaning

# For example, a list could contain the value of a stock over time,
# and the index could represent how many years into the past
stocks = [27, 26, 24, 22, 23, 25, 21]
#        Now,  1,  2,  3,  4,  5,  6 years ago

# Or, we can use MATH to give the numbers indirect meaning.
abcs = [0, 0, 0]
#       a  b  c

x = ord('a')
print(x)

# How to map 'a' to 0, 'b' to 1, 'c' to 2 without an if statement?
