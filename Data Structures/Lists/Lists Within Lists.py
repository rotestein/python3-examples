# In addition to having simple things in our lists like numbers, we can also have
# complex things, such as lists within lists!

"""
  | |   |   |   | |  | |   |   |   | |
  | | 5 |   |123| |  | |abc|   |cat| |
  |  ___     ___  |  |  ___     ___  |
  |               |  |               |
  |   0       1   |  |   0       1   |
   _______________    _______________ 

          0                  1
"""

# To create an empty list (without any buckets...)
listoflists = [ [5, 123], ['abc', 'cat'] ]

# To access an element, we need to give 2 indices
print(listoflists[0][1])
listoflists[0][1] += 10
print(listoflists[0][1])

# If we only use one index, we get back the list within that bucket
print(listoflists[1])

for word in listoflists[1]:
    for character in word:
        print(character)
