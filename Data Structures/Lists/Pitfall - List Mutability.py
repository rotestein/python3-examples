my_list = [1, 2, 3, 5, 7, 11, 13];

# Why doesn't this work?
my_list[0] = my_list[1]
my_list[1] = my_list[0]

# What does this do differently?
temp = my_list[0]
my_list[0] = my_list[1]
my_list[1] = temp
