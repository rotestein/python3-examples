# ALL of the following accomplish the task of iterating through a list.
my_list = [1, 2, 3, 5, 7, 11, 13];

idx = 0
while idx < len(my_list):
  print(my_list[idx])
  idx+=1

for idx in range(len(my_list)):
  print(my_list[idx])

for number in my_list:
  print(number)
