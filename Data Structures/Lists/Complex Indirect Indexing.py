results = [                               # indecies
    'Frog     1st in long jump',          # 0
    'Beaver   2nd in long jump',          # 1
    'Monkey   3rd in long jump',          # 2
    'Rabbit   1st in speedy walking',     # 3
    'Zebra    2nd in speedy walking',     # 4
    'Spider   3rd in speedy walking',     # 5
    'Elephant 1st in swimming',           # 6
    'Snake    2nd in swimming',           # 7
    'Penguin  3rd in swimming',           # 8
    'Fly      1st in snow art creation',  # 9
    'Giraffe  2nd in snow art creation',  # 10
    'Turtle   3rd in snow art creation'   # 11
  ]

# get all of the animals in first place
def getFirstPlace():
  return [results[0*3],results[1*3],results[2*3]] # write this in lab

def getSwimmers():
  return results[6: 6 + 3] # write this in lab

def whichPlace(index):
  places = ['1st', '2nd', '3rd']
  # How would you translate an animal's index into its place?
  # complete this halfway through lab

def whichEvent(index):
  events = ['long jump', 'speedy walking', 'swimming', 'snow art creation']
  # How would you translate an animal's index into its event?
  # complete this halfway through lab
