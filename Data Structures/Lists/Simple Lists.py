# Lists are like numbered buckets which contain other python objects

"""
   |   |   |   |   |   |   |   |   |   |
   | 5 |   |123|   |abc|   |cat|   |11 |
    ___     ___     ___     ___     ___ 

     0       1       2       3       4
"""

# To create an empty list (without any buckets...)
buckets = []

# To add a bucket with something in it
buckets += [5]

# You can add more than one thing at time
buckets += [123, 'abc', 'cat', 11]

# To see what is in a bucket
print(buckets[1])

# To change what is in a bucket THAT ALREADY EXISTS
buckets[1] = 'def'
print(buckets[1])

# If the bucket you are trying to use doesn't exist it errors

# THIS BREAKS
# buckets[5] = 12
# print(buckets[5])

