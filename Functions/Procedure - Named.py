import turtle

# Naming a procedure allows us to reuse the code multiple times.
# Instead of copying and pasting the code in Clover, I can just
# call it however many times I want.
def Clover():
    turtle.forward(100)
    turtle.circle(25)
    turtle.right(90)
    turtle.circle(25)
    turtle.right(90)
    turtle.circle(25)

Clover()
Clover()
Clover()
