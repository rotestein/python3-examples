def callThisFunction(function, argument):
    function(argument)

def thisIsAFunction(thisIsItsArgument):
    print('thisIsAFunction was called with argument', thisIsItsArgument)

callThisFunction(thisIsAFunction, 12345)
