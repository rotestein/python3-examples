def sayHi():
    print('Hello!')

def sayBi():
    print('Goodbye!')

def sayName():
    print('My name is Fritz!')

def sayUnknown():
    print('I don\'t understand...')

def sayMean():
    print('Siri is gross.')

# notice the lack of paranthesis, these
# are references to functions
responses = {
    'hi': sayHi,
    'bye': sayBi,
    'what is your name?': sayName,
    'Are you Siri?': sayMean,
    'unknown': sayUnknown
}

# P.S., this is how the grading script works
while True:
    i = input('> ')
    if i in responses:
        function = responses[i]
        function()
    else:
        responses['unknown']()
