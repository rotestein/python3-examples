
# Simple math functions
#   f(x) = x^2
#   g(x) = mx + b
def f(x):
    return x**2

def g(m, b, x):
    return m * x + b

for i in range(10):
    print("f(",i,") = ", f(i))

a = -1
b = 2
for i in range(5):
    print("g(", b, ",", a, ",", i ") = ", g(b, a, i))
