# It is important to understand where variables are defined 

# If you bind a variable outside of any function, it exists in
# the "global" space. Meaning you can (usually) access it from
# anywhere.
x = 1

# Since we defined x in the global space, we can access it inside of foo()
# HOWEVER, this is considered bad practice and you should not have your
# functions depend on global variables.
def foo():
    print(x)

foo() # What does this print?

# A better way to gain access to a variable is to use a parameter.
# In the following definition of foo, x is used as a parameter, and
# IS NOT RELATED to the x defined above.
def foo(x):
    print(x)

foo(2) # What does this print?

# However, if we rebind x in our function, it no longer has the value passed.
def foo(x):
    x = 2
    print(x)

foo(1) # What does this print?

print(x) # What does this print?
