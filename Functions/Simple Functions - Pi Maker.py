# When can I use the variable pi?
# (Where could I put the code `print(pi)'?)

# A
def pi_maker(n):
    x = 0
    # B
    pi = 3.14159
    l = []
    while x < n:
        l += [x * pi]
        x += 1
    # C
    return l

# D

pi_maker(10)

# E
