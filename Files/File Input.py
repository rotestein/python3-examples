def readThisFile(this_file):
    print("Using .readlines()")
    fin = open(this_file)
    lines_of_file = fin.readlines()
    for line in lines_of_file:
        print(line, end='')
    fin.close()

    print("Using .read()")
    fin = open(this_file)
    print(fin.read())
    fin.close()

    print("Using a while loop")
    fin = open(this_file)
    line = fin.readline()
    while line != '':
        print(line, end='')
        line = fin.readline()
    fin.close()

    print("Using a for loop")
    for line in open(this_file):
        print(line, end='')
    fin.close()

readThisFile("ex1.py")
print()
