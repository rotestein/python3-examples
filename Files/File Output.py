def writeIt(name, age):
    fout = open('test.txt', 'w')
    fout.write('Hi!\n')
    fout.write('My name is ' + name + ' ')
    fout.write('and I am ' + str(age) + ' years old.\n')
    fout.close()

def printIt(name, age):
    print('Hi!')
    print('My name is', name, end='')
    print('and I am', age, 'years old.')

printIt('Bob', 19)
writeIt('Bob', 19)
