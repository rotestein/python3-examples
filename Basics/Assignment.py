# In order to keep things around in python, we have to give them a name.
# This is called variable assignment and it exists in some form in all
# programming languages.

# Give the number 5 the name x so we can call it by x later:
x = 5

# Give the word 'apple' the name food:
food = 'apple'

print('x', x)
print('food', food)
print()

# If we use a name that already exists again, the thing that the name
# is labeling ("points to") changes.

x = 7
food = 'orange'

# This code is the same as above, but gives us a different result.
print('x', x)
print('food', food)
print()

# The way assignment works is to "evaluate" everything on the right
# side of the '=' operator FIRST and then apply the label on the left 
# to whatever is on the right AFTER evaulation.

# This means we can use the label on BOTH sides
x = x + 1
food = food + ', ' + 'banana'
print('x', x)
print('food', food)
