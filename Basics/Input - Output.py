# In order for programs to be useful, they need to get input and output.
# The easiest way to do this is from the user.

# The following prints the string "Enter something! " on the screen
# and waits for the user to type something and hit ENTER.
user_input = input("Enter something! ")
print("You entered", user_input, "which is a", type(user_input), ".")

# Input from the user is always given as a "string" of "characters".
# This means if we want to use them as something else, we need to
# convert it. See "Pitfall - Characters vs Numbers.py" for more.

user_number = input("Enter a number: ")
user_number = float(user_number)
print("You entered the number", user_number, "which is a", type(user_number), ".")

# Or to do this in one line
user_number = float(input("Enter a number: "))

# Once we have some input, we probably want to show it to the user.
# This is called outputing and is done with 'print'
print("You entered", user_number, ". I will now add 5 to your number.")
user_number = user_number + 5
print("The new number is", user_number)
