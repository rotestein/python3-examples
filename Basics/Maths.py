# Python allows us to do basic math out of the box

# Addition
ans = 3 + 5
print(ans)
print()

# Subtraction
ans = 3 - 5
print(ans)
print()

# Multiplication
ans = 2 * 4
print(ans)
print()

# Exponentiation
ans = 2 ** 4 # 16
print(ans)
print()

# Division (Floating point)
ans = 8 / 3
print(ans)
print()

# Integer Division
ans = 10 // 3 # 3
print(ans)
print()

# Division Remainder (Modulus)
ans = 10 % 3 # 1
print(ans)
print()
