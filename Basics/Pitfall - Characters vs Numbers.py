numeric = 5
notnumeric = "5"

# If you try to add to a numeric thing in python it works as expected
numeric = numeric + 1
print(numeric)

# If you try to add to a string representation of a number, it won't do what
# is expected. Comment this line out after you explain why it breaks and how
# to read the error.
notnumeric = notnumeric + 1

# Can do this though
notnumeric = notnumeric + "1"
print(notnumeric)
