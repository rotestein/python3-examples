# Be careful that your if statements are connected correctly.

# Say I want to check the value of a number and add to it if it's 
# a special value.

# What's wrong with the following code?

x = 1
if x == 1:
    x += 1
if x == 2:
    x += 2
if x == 4:
    x += 4
if x == 10:
    x += 10
else:
    x += 20

print(x)

# The problem is that the individual if statements ARE NOT CONNECTED. 
# In other words, each if statement starts a new if-elif-else block, so if the
# initial number is 1, the first three if statements execute.
# Finally, if x is not 10 when python reaches line 15, 20 is added, regardless
# of what x started at or whether any of the other statements were true.
