# If-elif-else statements allow us to have the code follow
# different paths depending on the answer to a yes/no question,
# i.e., the value of a boolean expression.

"""
    initial code
        |
        |
     if-else
       / \
   if /   \ else
     /     \
   code    code
   in      in
   if      else
     \    /
      \  /
       \/
 code after if-else
"""

x = 5

### START OF IF-ELSE BLOCK ###
if x > 0:
    print("The question was True.")
else:
    print("The question was False.")
### END OF IF-ELSE BLOCK ###

# In an if-elif-else block, only one branch will execute. To have 
# multiple branches, we use the elif keyword to link them together.

### START OF IF-ELIF-ELSE BLOCK ###
if x > 5 :
    print("The first question was True.")
elif x > 0 and x <= 5:
    print("The second question was True.")
else:
    print("Neither question was true.")
### END OF IF-ELIF-ELSE BLOCK ###

# Sometimes we don't want to do anything when the answer to the 
# question is false. In this case, we can omit the else block.

# For example, say we want to make sure x is always positive. If
# it's negative, we want to make it positive, otherwise we don't
# want to do anything.

### START OF IF BLOCK ###
if x < 0:
    x *= -1
### END OF IF BLOCK ###

print(x)
