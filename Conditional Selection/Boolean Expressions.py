# A boolean expression is the same as a mathematical expression.
# The only difference is that instead of all numbers being possible
# results, only the values True or False can be results.

# An example of a boolean expression
boolex1 = True and (True or False)

# This is similar to the mathematical expression
mathex1 = 5 * (2 + 3)

# Another example of a boolean expression, this
# time using variables and the > and <= operators
x = 5
boolex2 = (x > 0) and (x <= 10)

# One more example
boolex3 = not ((boolex1) and (boolex2))

print(boolex1)
print(boolex2)
print(boolex3)

# Boolean expressions are used to state a yes-or-no question and 
# we use boolean expressions in if statements and while loops

# Which statement will print?
if boolex3:
    print("boolex3 was True")
elif boolex2:
    print("boolex2 was True and boolex3 was False")
elif boolex1:
    print("boolex1 was True and boolex2 was False and boolex3 was False")
else:
    print("all expressions were True")
