# Simple while loop

# Start by initializing our iterator variable
i = 0 

# Then we pose a question in the form of a boolean expression,
# just like with an if statement. If the answer to the question
# is yes (True), our loop begins.
while i < 10:
  # Here we want to do something interesting with our loop.
  print("The variable i was", i, "which is less than 10...")

  # We need to increase our accumulator so that, eventually,
  # the answer to our question becomes false.
  i = i + 1

  # When python gets to the end of the loop, it goes
  # back to the question, and checks if it was true
  print("Heading back to the top now.")

# Once the answer to the question becomes false, the program goes on
print("Our loop finished!")

print()

# Here's another example where we sum up a bunch of random numbers
import random

# Always need to initialize our iterator
i = 0 

# This time, we'll keep a running total. Before we've added any numbers,
# we have a total of 0....
total = 0

# How many numbers do we want? Try changing this.
n = 5

while i < n:
  # Get a random number between 0 and 1
  randnum = random.random()
  print("Got", randnum)

  # Add the number to our running total
  total = total + randnum
  print("Total is now", total)

  # Don't forget to increment the iterator!
  i = i + 1
