import turtle

count = 50

turtle.penup()
turtle.goto(-200, -200)
turtle.setheading(45)
turtle.pendown()

while count > 0:
    x = turtle.xcor()
    y = turtle.ycor()

    turtle.dot()

    turtle.penup()
    turtle.goto(x + 8, y + 8)
    turtle.pendown()

    count -= 1

turtle.setheading(225)
turtle.forward((400**2 + 400**2)**(1/2))
